import axios from 'axios'
import Utils from './Utils'

class AxiosUtils {

    baseApi = Utils.baseApi;

    get(endPoint) {
        return get(this.baseApi + endPoint, null);
    }

    get(endPoint, options) {
        return axios.get(this.baseApi + endPoint, options);
    }

    post(endPoint) {
        return post(this.baseApi + endPoint, null);
    }

    post(endPoint, options) {
        return axios.post(this.baseApi + endPoint, options);
    }

}

export default AxiosUtils 