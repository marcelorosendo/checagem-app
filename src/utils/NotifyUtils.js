import { Notify } from 'quasar'

class NotifyUtils {

    position = 'top-right';

    notify(options) {
        Notify.create(options)
    }

    info(message) {
        Notify.create({
            color: 'info',
            message: message,
            position: this.position,
            icon: 'notifications',
            actions: [{
                icon: 'close', // optional
                noDismiss: true, // optional, v0.15.11+
                color: 'white'
            }],
        });
    }

    success(message) {
        Notify.create({
            color: 'positive',
            message: message,
            position: this.position,
            icon: 'done',
            actions: [{
                icon: 'close', // optional
                noDismiss: true, // optional, v0.15.11+
                color: 'white'
            }],
        });
    }

    error(message) {
        Notify.create({
            color: 'negative',
            message: message,
            position: this.position,
            icon: 'error',
            actions: [{
                icon: 'close', // optional
                noDismiss: true, // optional, v0.15.11+
                color: 'white'
            }],
        });
    }

    warning(message) {
        Notify.create({
            color: 'warning',
            message: message,
            position: this.position,
            icon: 'warning',
            actions: [{
                icon: 'close', // optional
                noDismiss: true, // optional, v0.15.11+
                color: 'white'
            }],
        });
    }


}

export default NotifyUtils;