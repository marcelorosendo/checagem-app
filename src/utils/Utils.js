var Utils = {
    baseApi: process.env.TEST
      ? 'https://spdata.jelastic.saveincloud.net/APLICACAO'
      : process.env.NODE_ENV === 'production'
        ? 'https://spdata-test.jelastic.saveincloud.net/APLICACAO'
        : 'http://localhost:8080/APLICACAO'
  }
  
  export default Utils
  