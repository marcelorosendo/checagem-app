import Service from './Service'
import NotifyUtils from '../utils/NotifyUtils'

class LoginService extends Service {

    login(tecnico) {
        let endPoint = '/login';

        return this.post(endPoint, tecnico)
            .then((response) => {
                let jwt = response.data
                this.setInLocalStorage('jwt', jwt);
            })
    }

}

export default LoginService;