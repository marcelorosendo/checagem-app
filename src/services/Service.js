import AxiosUtils from '../utils/AxiosUtils'
import { LocalStorage, SessionStorage } from 'quasar'
var jwt = require('jsonwebtoken');

class Service extends AxiosUtils {

    setInLocalStorage(key, value) {
        LocalStorage.set(key, value);
    }

    getFromLocalStorage(key){
        return LocalStorage.getItem(key);
    }


    setInSessionStorage(key, value) {
        SessionStorage.set(key, value);
    }

    getFromSessionStorage(key){
        return SessionStorage.getItem(key);
    }

    getJWT(){
        return this.getFromLocalStorage('jwt');
    }

    getDecodedJWT(){        
        var decoded = jwt.decode(this.getJWT());
        return decoded;
    }

    isLogged(){
        return this.getJWT() != null;
    }

    getVersion(){
        return this.getFromLocalStorage('version');
    }

    sair(){
        LocalStorage.clear();
    }

}

export default Service;