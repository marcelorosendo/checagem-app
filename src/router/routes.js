import Login from 'pages/Login.vue'
import Configuration from 'pages/Configuration.vue'


const routes = [
  {
    path: '/',
    component: Configuration,
    meta: {
      title: 'Login'
    },
    props:{default : true}
  },
  {
    path: '/app',
    component: () => import('layouts/Layout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue'),
        meta: {
          requiresAuth: true,
          title: 'Início',
          backButton: '/'
        }
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
