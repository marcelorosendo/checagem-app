import axios from 'axios'

import NotifyUtils from '../utils/NotifyUtils'
import Service from '../services/Service'

export default async ({ Vue }) => {
  Vue.prototype.$axios = axios

  let notify = new NotifyUtils();
  let service = new Service();

  axios.interceptors.request.use(

    (config) => {

      let token = service.getJWT();

      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`;
      }

      return config;
    },

    (error) => {
      return Promise.reject(error);
    }
  );


  axios.interceptors.response.use(function (response) {
    if (response != undefined && response.headers != undefined) {
      let version = response.headers.restversion;
      if (version != null && version != undefined) {
        //console.log(version);
        service.setInLocalStorage('version', version);
      }
    }

    return response;
  }, function (error) {

    let response = error.response;

    if (response != undefined && response.data != undefined) {
      notify.error(response.data.message)
    } else {
      notify.error("Ocorreu um erro inesperado na aplicação")
    }

    return Promise.reject(error);

  });
}
